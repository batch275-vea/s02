@extends('layouts.app')
@section('content')

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
    <div>
        <center><img height="400px" width="600px" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png"></center>
    </div>

        <h2 align="center" class="mt-5">Featured Posts:</h2>
        @foreach($posts as $post)
        <center><div class="card text-center my-2">
                <div class="card-body">
                    <h3 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}">
                            {{$post->title}}
                        </a>
                    </h>
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                </div>
        </div></center>
        @endforeach
</body>
</html>
@endsection